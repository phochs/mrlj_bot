# MR.LJ's super epic chat bot

[![pipeline status](https://gitlab.com/phochs/mrlj_bot/badges/main/pipeline.svg)](https://gitlab.com/phochs/mrlj_bot/-/commits/main)

This is the Twitch chatbot for MR.LJ's livestreams. It's built on Node.JS and offers all the features you'd want from a Twitch bot, including: basic commands you can set using a JSON file, OBS integration, terrible riddles and doing all this with a very high stability.

## Features

### Built in commands

The bot has the following commands built in:

* !cocktail: returns a recipe for a cocktail if an API key is provided
* !context: a mod only command to dump the Twitch context object to the log
* !ping: responds with a Pong
* !fact: gives a random fact, if an API key is provided
* !riddle: gives a random (and in my opinion horrible) riddle, if an API key is provided
* !so: gives a shout-out to a user (mod only)
* !version: responds with the current bot version (mod only)

### Web server

The bot has a built-in web server which provides Prometheus-styled metrics of the bot. These metrics are currently not persistent; they will reset when the bot is restarted. It currently offers the following metrics:

| Metric name                | Description                                |
|----------------------------|--------------------------------------------|
| mrlj_bot_messages_received | Total number of messages received          |
| mrlj_bot_messages_commands | Total number of messages that are commands |
| mrlj_bot_messages_sent     | Total number of messages sent              |

The metrics can be reached at: `http://localhost:8080/metrics`.

### Dynamic commands

The bot can execute dynamic commands, which are pulled from the `dynamicCommands.json` file inside the `res/` folder. These commands are automatically updated whenever the file is changed, therefor the bot doesn't need to be restarted for any new commands to take effect.

### OBS integration

The bot can integrate with an OBS client and execute commands to manipulate sources inside OBS. Currently, it can either cycle a source (turning it on, and then off again), or toggle it (turning it either on or off, depending on it's previous state).

These commands work the same as other dynamic commands. They reside in `OBSCommands.json` and are automatically updated when the file is changed. 

## Installation

There are two main methods of running the MR.LJ bot. You can either download the code from Gitlab, or use the provided Docker image.

### Docker

Run the Docker image using the command below:

```
$ docker run -d \
-v mrlj_bot_res:/bot/res \
-v mrlj_bot_logs:/bot/logs \
-e CLIENT_USERNAME='value' \
-e CLIENT_ID='value' \
-e CLIENT_SECRET='value' \
-e CLIENT_CHANNEL='value' \
mrlj/mrlj_bot:latest
```

Set the ENV values accordingly, and you're good to go! Note that the container will not run successfully if you don't provide a `dynamicCommands.json` file. You will also need to specify a `OBScommands.json` file if the OBS integration is enabled. See Configuration for more details.

### Manual

1. Clone the project from Gitlab using `git clone https://gitlab.com/phochs/mrlj_bot.git`
2. Go to the directory and run `npm install` to install dependencies
3. Copy the `.env.example` file to `.env` and set the values accordingly
4. Copy the `res/dynamicCommands.example.json` file to `res/dynamicCommands.json` and fill it with useful commands
5. Copy the `res/OBSCommands.example.json` file to `res/OBSCommands.json` and fill it with useful commands
6. Run the bot with `node main.js`

## Configuration

The bot uses a set of environment variables for general configuration. These variables can be set by specifying a .env
file in the root folder of the bot, or by passing them via Bash, Docker or something alike. Apart from those, the bot
uses JSON files for any dynamic commands. These JSON files reside within the res/ folder of the bot.

### ENV values

| Value                      | Required? | Default value | Description                                                                                                                        |
|----------------------------|-----------|---------------|------------------------------------------------------------------------------------------------------------------------------------|
| CLIENT_USERNAME            | Yes       | None          | The username the bot uses in Twitch chat                                                                                           |
| CLIENT_ID                  | Yes       | None          | The client ID of the Twitch bot/user, provided by Twitch                                                                           |
| CLIENT_SECRET              | Yes       | None          | The client secret of the Twitch bot/user, provided by Twitch                                                                       |
| CLIENT_CHANNEL             | Yes       | None          | The channel the bot will join once started. Currently, only a single channel is supported                                          |
| OBS_ENABLED                | No        | False         | If the OBS integration is enabled                                                                                                  |
| OBS_ADDRESS                | No        | None          | The Websocket address for the OBS integration, for example: ws://localhost:4455                                                    |
| OBS_MAX_ATTEMPTS           | No        | 10            | The maximum number of reconnect attempts before the bot will stop trying to connect to OBS. Set it to -1 to keep trying infinitely |
| OBS_RECONNECT_TIMEOUT      | No        | 1000          | The amount of time the bot will wait before trying to reconnect to OBS, in milliseconds                                            |
| TIMEOUT_BROADCASTER_EXEMPT | No        | False         | If broadcasters are exempt from command timeouts by default                                                                        |
| TIMEOUT_MOD_EXEMPT         | No        | False         | If moderators are exempt from command timeouts by default                                                                          |
| TIMEOUT_VIP_EXEMPT         | No        | False         | If VIPs are exempt from command timeouts by default                                                                                |
| API_NINJA_KEY              | No        | None          | The API key for API Ninja, used by the riddles, cocktails and random facts. Required for those to function                         |
| COMMAND_COCKTAIL_TIMEOUT   | No        | 0             | The cooldown for the cocktail command, in milliseconds. The value of 0 will only ensure it's not run concurrently                  |
| COMMAND_FACT_TIMEOUT       | No        | 0             | The cooldown for the random fact command, in milliseconds. The value of 0 will only ensure it's not run concurrently               |
| COMMAND_RIDDLE_TIMEOUT     | No        | 0             | The cooldown for the riddle command, in milliseconds. The value of 0 will only ensure it's not run concurrently                    |
| COMMAND_CONTEXT_TIMEOUT    | No        | 0             | The cooldown for the context dump command, in milliseconds.                                                                        |
| COMMAND_PING_TIMEOUT       | No        | 0             | The cooldown for the ping command, in milliseconds.                                                                                |
| COMMAND_SO_TIMEOUT         | No        | 0             | The cooldown for the shout-out command, in milliseconds.                                                                           |
| COMMAND_VERSION_TIMEOUT    | No        | 0             | The cooldown for the version command, in milliseconds.                                                                             |

### JSON files

The JSON files in `res/` keep the dynamic commands for both "normal" commands, and OBS commands. The file `dynamicCommands.json` keeps all commands that just reply with a response text. These can be randomized by offering multiple options for responses. The `OBSCommands.json` file specifies the commands which interact with OBS. These can also have (randomized) text responses.

The general structure of these files is as follows:

```json
{
  "commands": [
    {
      "command": "!commandName",
      "timeout": 5000,
      "assumeFailed": 3000,
      "responses": []
    }
  ]
}
```

Each JSON file can have an unlimited number of entries within the `commands` array. Every command has the following attributes:

* `command` (required): the name of the command, including the prefix (like !)
* `timeout` (optional): Any timeout that should be active on the command, in milliseconds. Omit this attribute if you don't want any timeout. Set it to 0 to prevent multiple users running this command at the same time
* `assumeFailed` (optional): How long the timeout should wait until it will assume the command has failed executing and will allow a new execution. Omit this value to keep the default value (2000 ms). Set to -1 to disable it.
* `responses` (required): A list of all valid responses for the command. The content of this depends on what the commands are used for. More on that below.
* `timer` (optional): Instructions for sending the command output at an interval to the chat channel. If used, an `interval` attribute is required, an `messages` attribute is optional

**Dynamic command responses**

Dynamic commands are simple Twitch command that give a text response in the chat. The `responses` array contains a list of strings, of which 1 is picked at random to send to the chat. Setting the timeout of these commands to 0 won't do anything, because these commands finish instantly. You can set a timeout higher than 0 to give the command a cooldown.

Below is an example of a valid dynamic command:

```json
{
  "commands": [
    {
      "command": "!lurk",
      "timeout": 5000,
      "responses": [
        "Have fun mighty scavenger!",
        "Don't have too much fun 😏"
      ]
    }
  ]
}
```

**OBS command responses**

OBS commands can be set dynamically, but interact with an OBS instance. The `responses` array contains a list of all OBS actions that will be performed. These actions will be performed in sequence, 1 at a time. Each response may have an attribute called `messages`, which contains a list off messages of which a random message will be sent back to the chat.

Currently, the following type of OBS action types are supported:

*sourceCycle*

Turns a specified source on for a certain duration, and then off again. This requires a `scene` and `source` attribute.
Below is an example of a valid cycle response:

```json
{
  "commands": [
    {
      "command": "!scare",
      "timeout": 5000,
      "responses": [
        {
          "scene": "Overlay",
          "source": "Scare-crow",
          "type": "sourceCycle",
          "duration": 500,
          "messages": [
            "Boooo!",
            "Got ya!"
          ]
        }
      ]
    }
  ]
}
```

*sourceToggle*

Toggles a specified source. So if the source is currently off, it will turn on, and vice versa. This requires a `scene` and `source` attribute.
Below is an example of a valid toggle response:

```json
{
  "commands": [
    {
      "command": "!counter",
      "timeout": 5000,
      "responses": [
        {
          "scene": "Overlay",
          "source": "counter-source",
          "type": "sourceToggle",
          "messages": [
            "Yay"
          ]
        }
      ]
    }
  ]
}
```

*sourceEnable / sourceDisable*

Switches a specified source on (sourceEnable) or off (sourceDisable). This requires a `scene` and `source` attribute.
Below is an example of a valid source enable response:

```json
{
  "commands": [
    {
      "command": "!counter-on",
      "timeout": 5000,
      "responses": [
        {
          "scene": "Overlay",
          "source": "counter-source",
          "type": "sourceEnable"
        }
      ]
    }
  ]
}
```

*sceneSwitch*

Switches the program output to a different scene. This requires a `scene` attribute.
Below is an example of a valid scene switch response:

```json
{
  "commands": [
    {
      "command": "!credits",
      "timeout": 5000,
      "responses": [
        {
          "scene": "The End",
          "type": "sceneSwitch",
          "messages": [
            "Goodbye"
          ]
        }
      ]
    }
  ]
}
```

*pause*

Does nothing for a specified amount of time. This requires a `duration` attribute. A pause response can still send a message as a text response.
Below is an example of a valid pause response, in this case followed by a source toggle:

```json
{
  "commands": [
    {
      "command": "!counter",
      "timeout": 5000,
      "responses": [
        {
          "duration": 1500,
          "type": "pause"
        },
        {
          "scene": "Overlay",
          "source": "counter-source",
          "type": "sourceToggle",
          "messages": [
            "Yay"
          ]
        }
      ]
    }
  ]
}
```

*null*

Literally does nothing. But it can optionally send a text response back to the chat. This can be used, for example, if you want to send a message *after* another response has finished, because text responses are sent before the OBS integration is executed.
Below is an example of a valid null response:

```json
{
  "commands": [
    {
      "command": "!whatsup",
      "timeout": 5000,
      "responses": [
        {
          "type": "null",
          "messages": [
            "Yo"
          ]
        }
      ]
    }
  ]
}
```

**Dynamic command timers**

All dynamic commands can be executed at an interval. This is done by adding a `timer` attribute to the command in the JSON file. This contains two attributes:

* `interval` (required): The amount of time between messages, in milliseconds
* `messages` (optional): The minimum amount of messages between two executions of the command. The timer will only send an automated response when both these criteria are met. If `messages` is not set, it will default to 0

Below is an example of a command with a timer:

```json
{
  "commands": [
    {
      "command": "!whoami",
      "timeout": 10000,
      "responses": [
        "phochs"
      ],
      "timer": {
        "interval": 20000,
        "messages": 30
      }
    }
  ]
}
```

## General tips

* Every command can have a timeout, both built in and dynamic ones. A timeout of 0 will ensure the command can only be run once at a time. Other users will have to wait until the command has finished.
* Every timeout has an `assumeFailed` property. After this time, the timeout will assume the command has failed but not called home again, and will allow a new execution. This can be set to -1 to disable it.

## Updating

### Migrating from 0.1.1 to 0.2.0

Between versions 0.1.1 and 0.2.0 the structure of the OBSCommands.json file has changed significantly. Please check this readme for the new syntax.

### Docker

Simply pull the latest image from the Docker hub and restart the container to get the latest version. This process can be automated by using something like [Watchtower](https://github.com/containrrr/watchtower).

### Manual

Go to the project folder and run `git pull` to get the latest version of the code.