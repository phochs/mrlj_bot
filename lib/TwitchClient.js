const EventEmitter = require('events');
const tmi = require('@twurple/auth-tmi');
const {RefreshingAuthProvider} = require('@twurple/auth');
const fs = require('fs');
const log4js = require("log4js");
const logger = log4js.getLogger();

class TwitchClient extends EventEmitter {
    constructor() {
        super();

        this.authProvider = null;
        this.tokenData = null;
        this.client = null;
    }

    init() {
        return new Promise((resolve, reject) => {
            this.tokenData = JSON.parse(fs.readFileSync('./tokens.json', 'UTF-8').toString());

            this.authProvider = new RefreshingAuthProvider({
                clientId: process.env.CLIENT_ID,
                clientSecret: process.env.CLIENT_SECRET,
                onRefresh: this._refreshToken,
            }, this.tokenData);

            this.client = new tmi.Client({
                options: {debug: true, messagesLogLevel: 'info'},
                connection: {
                    reconnect: true,
                    secure: true
                },
                authProvider: this.authProvider,
                channels: [process.env.CLIENT_CHANNEL],
                logger: logger,
            });

            this.client.on('message', (channel, context, message, self) => {
                this._onMessageHandler(channel, context, message, self);
            });
            this.client.on('connected', (address, port) => {
                this._onConnectedHandler(address, port);
            });

            this.client.connect().then((data) => {
                resolve(data);
            }).catch((e) => {
                reject(e);
            });
        });
    }

    say(channel, message) {
        this.client.say(channel, message);
    }

    _refreshToken(newTokenData) {
        this.tokenData = newTokenData;

        fs.writeFile('./tokens.json', JSON.stringify(newTokenData, null, 4), 'UTF-8', () => {
            logger.debug('Refreshed Twitch token');
        });
    }

    _onMessageHandler(channel, context, message, self) {
        if (self)
            return;

        this.emit('message', {
            channel,
            context,
            message
        });
    }

    _onConnectedHandler(address, port) {
        logger.info(`Twitch client connected on ${address}:${port}`);
    }
}

module.exports = TwitchClient;