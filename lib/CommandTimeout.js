const TIMEOUT_BROADCASTER_EXEMPT = process.env.COMMAND_PING_TIMEOUT ? process.env.COMMAND_PING_TIMEOUT.toLowerCase() === 'true' : false;
const TIMEOUT_MOD_EXEMPT = process.env.TIMEOUT_MOD_EXEMPT ? process.env.TIMEOUT_MOD_EXEMPT.toLowerCase() === 'true' : false;
const TIMEOUT_VIP_EXEMPT = process.env.TIMEOUT_VIP_EXEMPT ? process.env.TIMEOUT_VIP_EXEMPT.toLowerCase() === 'true' : false;

/**
 * Handles the cooldown and concurrency of Twitch commands.
 */
class CommandTimeout {
    constructor() {
        /**
         * Whether the timeout is active
         * @type {boolean}
         */
        this.active = false;

        /**
         * How long viewers have to wait after the command has finished executing, in milliseconds. Setting this to 0 or
         * lower will only prevent the command from running concurrently (so it will only allow 1 execution of the
         * command at any moment)
         * @type {number}
         */
        this.timeout = 0;

        /**
         * Assume the command has failed executing after this amount of time, in milliseconds. Set to -1 to disable
         * @type {number}
         */
        this.assumeFailed = 2000;

        /**
         * Which user roles are exempt from timeouts of the command
         * @type {{mod: boolean, broadcaster: boolean, vip: boolean}}
         */
        this.exceptions = {
            broadcaster: TIMEOUT_BROADCASTER_EXEMPT,
            mod: TIMEOUT_MOD_EXEMPT,
            vip: TIMEOUT_VIP_EXEMPT,
        };

        /**
         * The last time the command was started, in milliseconds
         * @type {?number}
         * @protected
         */
        this._lastTimeStarted = null;

        /**
         * The last time the command has finished. The timeout starts counting after the command finished
         * @type {?number}
         * @protected
         */
        this._lastTimeFinished = null;
    }

    /**
     * CHeck if the command can be executed base on the timeout.
     * @param {UserContext} context
     * @return {boolean}
     */
    test(context) {
        // Timeout isn't active
        if (!this.active)
            return true;

        // user is exempt
        if (context.broadcaster === true && this.exceptions.broadcaster === true)
            return true;
        if (context.mod === true && this.exceptions.mod === true)
            return true;
        if (context.vip === true && this.exceptions.vip === true)
            return true;

        let now = this._getNow();

        // Command is currently running
        if (this._lastTimeStarted > this._lastTimeFinished) {
            // Check if the command should be assumed to have failed
            if (this.assumeFailed > -1 && this._lastTimeStarted + this.assumeFailed < now) {
                this._lastTimeFinished = this._lastTimeStarted + this.assumeFailed;
            }
            // If it's not assumed to have failed
            else {
                return false;
            }
        }

        // Check if command is in cooldown
        return this._lastTimeFinished + Math.max(0, this.timeout) < now;
    }

    /**
     * Start executing the command
     */
    start() {
        if (this.active)
            this._lastTimeStarted = this._getNow();
    }

    /**
     * Finish executing the command. If the command executes instantly, only run this.finish() and ignore this.start()
     */
    finish() {
        if (this.active)
            this._lastTimeFinished = this._getNow();
    }

    /**
     * Get the current UNIX time
     * @return {number} The current UNIX time in milliseconds
     * @protected
     */
    _getNow() {
        return new Date().getTime();
    }
}

module.exports = CommandTimeout;