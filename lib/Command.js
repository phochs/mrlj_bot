const CommandTimeout = require('./CommandTimeout');

/**
 * Base class for all commands. These commands all reside inside the commands/ folder and extend this class.
 * Inside, they have a test() and execute() method. test() gets called every time a chat messages comes in.
 * If it's valid for that command, it will return true, and subsequently this.execute() method will be called.
 * A command class can be home to 1 or more actual commands the user can use.
 */
class Command {
    /**
     * @param {TwitchClient} client
     */
    constructor(client) {
        this._client = client;
        this.commandTimeout = new CommandTimeout();
    }

    /**
     * Initialize the command. These are executed sequentially. Initializing a command isn't always needed.
     * @return {Promise}
     */
    init() {
        return new Promise((resolve) => {
            resolve();
        });
    }

    /**
     * Checks if the input string is a valid command
     * @param {String} command The command the user entered, including any prefixes like an exclamation mark
     * @param {String[]} args A list of all arguments passed by the user
     * @param {UserContext} context The user context object
     * @return {boolean} True if the command is valid for this class, false if not
     */
    test(command, args, context) {
        return this.commandTimeout.test(context) && this._commandTest(command, args, context);
    }

    /**
     * Checks if the input string is a valid command. To be overwritten by the command implementation
     * @param {String} command The command the user entered, including any prefixes like an exclamation mark
     * @param {String[]} args A list of all arguments passed by the user
     * @param {UserContext} context The user context object
     * @return {boolean} True if the command is valid for this class, false if not
     * @abstract
     */
    _commandTest(command, args, context) {
        return false;
    }

    /**
     * Run the command. This happens after it has been tested
     * @param {String} command The same command as used in test(). This is passed again because some Command classes can host multiple commands
     * @param {Array} args A list of all arguments passed by the user
     * @param {String} channel The channel the user sent a message on
     * @param {UserContext} context The user context object
     * @param {Function} respond A callback function that can be used to send text to the channel. It takes a single argument: the text as a string
     * @abstract
     */
    execute(command, args, channel, context, respond) {
    }
}

module.exports = Command;