class UserContext {
    constructor(userContext) {
        this.raw = {};

        this.displayName = null;
        this.username = null;

        this.subscriber = false;
        this.mod = false;
        this.vip = false;
        this.broadcaster = false;

        if (userContext)
            this.setContext(userContext);
    }

    setContext(userContext) {
        if (!userContext.badges)
            userContext.badges = {};

        this.raw = userContext;

        this.vip = userContext.badges.vip === '1';
        this.mod = userContext.mod;

        if (userContext.badges.broadcaster === '1') {
            this.broadcaster = true;
            this.mod = true;
        } else {
            this.broadcaster = false;
        }

        this.subscriber = userContext.subscriber;
        this.username = userContext.username;
        this.displayName = userContext.displayName;

        return true;
    }
}

module.exports = UserContext;