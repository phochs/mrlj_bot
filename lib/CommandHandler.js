const CommandHelper = require('./CommandHelper');
const UserContext = require('./UserContext');
const fs = require('fs');
const path = require('path');
const async = require('async');
const log4js = require("log4js");
const logger = log4js.getLogger();
const promClient = require('prom-client');

const commandsDir = path.join(__dirname, 'commands');

/**
 * The command handler receives all messages from the Twitch client, and tries to execute these as commands.
 * Each command has its own file inside the commands/ folder. These files have a class that extends Command.
 * This class auto-detects all files within the commands/ folder, and tries to initialize them.
 */
class CommandHandler {
    constructor(twitchClient) {
        this.twitchClient = twitchClient;
        this.commands = [];
        this.metrics = {};
    }

    /**
     * Initialize the command handler; loading all the commands and setting up a listener for new Twitch  chat messages
     * @return {Promise<unknown>}
     */
    init() {
        this.twitchClient.on('message', (data) => {
            this._executeCommand(data.channel, data.context, data.message);
        });

        try {
            this.metrics.messagesReceived = new promClient.Counter({
                name: 'mrlj_bot_messages_received',
                help: 'Total number of messages received',
                labelNames: ['channel', 'username', 'mod', 'subscriber', 'message_type'],
            });
            this.metrics.messagesSent = new promClient.Counter({
                name: 'mrlj_bot_messages_sent',
                help: 'Total number of messages sent',
                labelNames: ['channel', 'command'],
            });
            this.metrics.messagesCommands = new promClient.Counter({
                name: 'mrlj_bot_messages_commands',
                help: 'Total number of messages that are commands',
                labelNames: ['channel', 'command', 'arguments'],
            });
        } catch (e) {
            logger.warn(e);
        }

        return this._loadCommands();
    }

    /**
     * Loads all command files from the commands/ folder. Each file has a single class, extending the Command class.
     * See the Command class for more information.
     * @return {Promise<unknown>}
     * @protected
     */
    _loadCommands() {
        return new Promise((resolve, reject) => {
            fs.readdir(commandsDir, (e, files) => {
                if (e) {
                    reject(e);

                    return;
                }

                async.each(files, (file, callback) => {
                    try {
                        if (!file.endsWith('.js'))
                            callback();
                        else {
                            let fullPath = path.join(commandsDir, file);

                            if (!fs.lstatSync(fullPath).isFile())
                                callback();
                            else {
                                let Command = require(fullPath);
                                let command = new Command(this.twitchClient);
                                let promise = command.init();

                                if (promise) {
                                    promise.then(() => {
                                        logger.debug(`Finished initializing ${file}`);
                                        this.commands.push(command);

                                        callback();
                                    }).catch(e => {
                                        logger.debug(`Could not initialize ${file}`);
                                        logger.log(e);

                                        callback();
                                    });
                                } else {
                                    callback();
                                }
                            }
                        }
                    } catch (e) {
                        logger.error(e);

                        callback();
                    }
                }, e => {
                    if (e)
                        logger.log(e);

                    resolve();
                });
            });
        });
    }

    /**
     * Gets called every time a message comes in. It will try to parse this message as a command, by checking each with
     * Command object if the input is a valid command
     * @param {String} channel The channel the message was sent in
     * @param {Object} context The user context object
     * @param {String} message The message a user sent
     * @protected
     */
    _executeCommand(channel, context, message) {
        try {
            if (this.metrics.messagesReceived) {
                this.metrics.messagesReceived.labels({
                    channel: channel,
                    username: context.username,
                    mod: context.mod,
                    subscriber: context.subscriber,
                    'message_type': context['message-type'],
                }).inc();
            }
        } catch (e) {
            logger.warn(e);
        }

        try {
            /**
             * An object of the parsed version of the user input, containing the command (first word), and it's arguments
             * @type {{args: [String], command: String}}
             */
            let commandSet = CommandHelper.parseCommandString(message);
            let userContext = new UserContext(context);

            // Loop through all commands
            for (let i = 0; i < this.commands.length; i++) {
                /**
                 * @type {Command}
                 */
                let commandObject = this.commands[i];

                // If the user input is valid for this command
                if (commandObject.test(commandSet.command, commandSet.args, userContext)) {
                    try {
                        logger.debug(`Found match for ${commandSet.command}`);

                        commandObject.execute(commandSet.command, commandSet.args, channel, userContext, (response) => {
                            logger.debug(`Sending response for ${commandSet.command}`);

                            if (response) {
                                try {
                                    response = response.toString();
                                    this.twitchClient.say(channel, response);

                                    if (this.metrics.messagesSent) {
                                        this.metrics.messagesSent.labels({
                                            channel: channel,
                                            command: commandSet.command,
                                        }).inc();
                                    }
                                } catch (e) {
                                    logger.error(e);
                                }
                            }
                        });

                        if (this.metrics.messagesCommands) {
                            this.metrics.messagesCommands.labels({
                                channel: channel,
                                command: commandSet.command,
                                arguments: commandSet.args.length
                            }).inc();
                        }

                        // Stop looking for valid commands when a match is found
                        break;
                    } catch (e) {
                        logger.error(e);
                    }
                }
            }
        } catch (e) {
            logger.error(e);
        }
    }
}

module.exports = CommandHandler;