const Command = require('../Command');
const log4js = require("log4js");
const logger = log4js.getLogger();

const COMMAND_CONTEXT_TIMEOUT = parseInt(process.env.COMMAND_CONTEXT_TIMEOUT) || 0;

/**
 * Internal command to dump the user context in the console log. This is not saved in the main log file
 */
class DumpContextCommand extends Command {
    constructor() {
        super();

        this.commandTimeout.active = true;
        this.commandTimeout.timeout = COMMAND_CONTEXT_TIMEOUT;
    }

    _commandTest(command, args, context) {
        return command === '!context' && context.mod === true;
    }

    execute(command, args, channel, context, respond) {
        respond(`Look at the log you dummy`);

        logger.info(context);
    }
}

module.exports = DumpContextCommand;