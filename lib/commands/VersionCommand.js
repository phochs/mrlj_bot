const Command = require('../Command');

const COMMAND_VERSION_TIMEOUT = parseInt(process.env.COMMAND_VERSION_TIMEOUT) || 0;

/**
 * Returns the current version of the bot. This action is only available for mods.
 */
class VersionCommand extends Command {
    constructor() {
        super();

        this.commandTimeout.active = true;
        this.commandTimeout.timeout = COMMAND_VERSION_TIMEOUT;
    }

    _commandTest(command, args, context) {
        return command === '!version';
    }

    execute(command, args, channel, context, respond) {
        if (!context.mod) {
            respond(`Do you look like a mod, @${context.username}???`);
            return false;
        }

        respond(`Currently running version ${process.env.APP_VERSION}!`);
    }
}

module.exports = VersionCommand;