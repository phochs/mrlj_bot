const Command = require('../Command');

const COMMAND_SO_TIMEOUT = parseInt(process.env.COMMAND_SO_TIMEOUT) || 0;

/**
 * Performs a shout-out for a selected user. This user is passed as the first parameter.
 * This action is only available for mods.
 */
class SOCommand extends Command {
    constructor() {
        super();

        this.commandTimeout.active = true;
        this.commandTimeout.timeout = COMMAND_SO_TIMEOUT;
    }

    _commandTest(command, args, context) {
        return command === '!so';
    }

    execute(command, args, channel, context, respond) {
        let username = args[0].replace('@', '');

        if (username === process.env.CLIENT_USERNAME) {
            respond('You want to shoutout... me?? *blushes* Well thank you! <3');

            setTimeout(() => {
                respond('Ohh yea, it\'s gitlab.com/phochs/mrlj_bot/ btw');
            }, 2500);
            return true;
        }

        if (!context.mod) {
            respond(`Do you look like a mod, @${context.username}???`);
            return false;
        }

        respond(`Check out @${username} at twitch.tv/${username}`);
    }
}

module.exports = SOCommand;