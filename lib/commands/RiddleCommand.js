const Command = require('../Command');
const fetch = require('node-fetch');
const log4js = require("log4js");
const logger = log4js.getLogger();

const COMMAND_RIDDLE_TIMEOUT = parseInt(process.env.COMMAND_RIDDLE_TIMEOUT) || 0;

/**
 * Gives a (usually terrible) riddle, using the API from api-ninjas.com. It gives the answer after a delay, based on the
 * message length.
 */
class RiddleCommand extends Command {
    constructor() {
        super();

        this.commandTimeout.active = true;
        this.commandTimeout.timeout = COMMAND_RIDDLE_TIMEOUT;
        this.commandTimeout.assumeFailed = 30000;
    }

    _commandTest(command, args, context) {
        return command === '!riddle';
    }

    execute(command, args, channel, context, respond) {
        if (!process.env.API_NINJA_KEY)
            return false;

        this.commandTimeout.start();
        respond('Hmmm...');

        fetch(`https://api.api-ninjas.com/v1/riddles`, {
            headers: {
                'X-Api-Key': process.env.API_NINJA_KEY,
            }
        }).then(res => res.json())
            .then(json => {
                let riddle = null;
                if (json instanceof Array) {
                    if (json.length > 0) {
                        riddle = json[Math.floor(Math.random() * json.length)];
                    }
                }

                if (riddle) {
                    respond(`*${riddle.title}*: ${riddle.question}`);

                    let timeOut = Math.min(riddle.question.length * 100, 15000);

                    setTimeout(() => {
                        respond(`*${riddle.title}*: ${riddle.answer}`);
                        this.commandTimeout.finish();
                    }, timeOut);
                } else {
                    respond('I\'m out');
                    this.commandTimeout.finish();
                }
            })
            .catch(e => {
                respond('I\'m out');
                this.commandTimeout.finish();

                logger.error(e);
            });
    }
}

module.exports = RiddleCommand;