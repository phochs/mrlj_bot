const Command = require('../Command');
const fetch = require('node-fetch');
const random = require('random');
const log4js = require("log4js");
const logger = log4js.getLogger();

const COMMAND_COCKTAIL_TIMEOUT = parseInt(process.env.COMMAND_COCKTAIL_TIMEOUT) || 0;

/**
 * Gives a recipe for a cocktail, using the API from api-ninjas.com.
 */
class CocktailCommand extends Command {
    constructor() {
        super();

        this.commandTimeout.active = true;
        this.commandTimeout.timeout = COMMAND_COCKTAIL_TIMEOUT;
        this.commandTimeout.assumeFailed = 10000;
    }

    _commandTest(command, args, context) {
        return command === '!cocktail';
    }

    execute(command, args, channel, context, respond) {
        if (!process.env.API_NINJA_KEY)
            return false;

        this.commandTimeout.start();
        respond('Hmmm...');

        let search = args.join('+');

        // Get a list of possible cocktails based on the user input
        fetch(`https://api.api-ninjas.com/v1/cocktail?name=${search}`, {
            headers: {
                'X-Api-Key': process.env.API_NINJA_KEY,
            }
        }).then(res => res.json())
            .then(json => {
                let cocktail = null;
                if (json instanceof Array) {
                    if (json.length > 0) {
                        cocktail = json[random.int(0, json.length - 1)]; // Select a random cocktail from the list it returned
                    }
                }

                // If a cocktail was found, format its data and send it to the chat
                if (cocktail) {
                    let ingredients = cocktail.ingredients.slice(0, cocktail.ingredients.length - 1).join(', ') + ", and " + cocktail.ingredients.slice(-1);

                    respond(`Alrighty! We're making a ${cocktail.name}! First, take ${ingredients}.`);
                    respond(`${cocktail.instructions} Enjoy!`);
                    this.commandTimeout.finish();
                } else {
                    respond('Sorry... dudn\'t finf anythigg');
                    this.commandTimeout.finish();
                }
            })
            .catch(err => {
                respond('Sorry... dudn\'t finf anythigg');
                this.commandTimeout.finish();

                logger.error(err);
            });
    }
}

module.exports = CocktailCommand;