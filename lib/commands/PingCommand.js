const Command = require('../Command');

const COMMAND_PING_TIMEOUT = parseInt(process.env.COMMAND_PING_TIMEOUT) || 0;

/**
 * Performs a simple ping-pong, including the response time. This time is not perfectly accurate though, because it's
 * mixing Twitch server time and local server time.
 */
class PingCommand extends Command {
    constructor() {
        super();

        this.commandTimeout.active = true;
        this.commandTimeout.timeout = COMMAND_PING_TIMEOUT;
    }

    _commandTest(command, args, context) {
        return command === '!ping';
    }

    execute(command, args, channel, context, respond) {
        this.commandTimeout.finish();
        respond(`Pong!`);
    }
}

module.exports = PingCommand;