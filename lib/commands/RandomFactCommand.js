const Command = require('../Command');
const fetch = require('node-fetch');
const log4js = require("log4js");
const logger = log4js.getLogger();

const COMMAND_FACT_TIMEOUT = parseInt(process.env.COMMAND_FACT_TIMEOUT) || 0;

class RandomFactCommand extends Command {
    constructor() {
        super();

        this.commandTimeout.active = true;
        this.commandTimeout.timeout = COMMAND_FACT_TIMEOUT;
        this.commandTimeout.assumeFailed = 10000;
    }

    _commandTest(command, args, context) {
        return command === '!fact';
    }

    execute(command, args, channel, context, respond) {
        if (!process.env.API_NINJA_KEY)
            return false;

        this.commandTimeout.start();
        fetch('https://api.api-ninjas.com/v1/facts', {
            headers: {
                'X-Api-Key': process.env.API_NINJA_KEY,
            }
        }).then(res => res.json())
            .then(json => {
                let foundAFact = false;
                if (json instanceof Array) {
                    if (json[0].fact) {
                        foundAFact = true;
                        respond(`Here's a fact: ${json[0].fact}`);
                        this.commandTimeout.finish();
                    }
                }

                if (!foundAFact) {
                    respond('Sorry, couldn\'t come up with anything');
                    this.commandTimeout.finish();
                }
            })
            .catch(e => {
                respond('Sorry, couldn\'t come up with anything');
                this.commandTimeout.finish();

                logger.error(e);
            });
    }
}

module.exports = RandomFactCommand;