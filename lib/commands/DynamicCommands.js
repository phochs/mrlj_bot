const MultiCommand = require('../MultiCommand');
const random = require('random');

/**
 * Manages all dynamic commands as defined in res/dynamicCommands.json. This file can be edited, and it will be reloaded as soon as it's saved.
 * This supports multiple responses for each command. If those are present, it will select a random one from the list.
 */
class DynamicCommands extends MultiCommand {
    constructor(client) {
        super(client);

        this._useFile = true;
        this._commandsFile = 'res/dynamicCommands.json';
    }

    /**
     * Execute a dynamic command
     * @param {boolean|{index: number, command: Object}} dynamicCommand
     * @param {String[]} args A list of all arguments passed by the user
     * @param {String} channel The channel where the message was sent
     * @param {UserContext} context The user context object
     * @param {function} respond
     * @protected
     * @abstract
     */
    _executeDynamicCommand(dynamicCommand, args, channel, context, respond) {
        this._commands[dynamicCommand.index]._commandTimeout.finish();

        let response = '';

        if (dynamicCommand.command.responses instanceof Array)
            response = dynamicCommand.command.responses[random.int(0, dynamicCommand.command.responses.length - 1)];
        else if (typeof dynamicCommand.command.responses === 'string')
            response = dynamicCommand.command.responses;
        else if (typeof dynamicCommand.command.response === 'string')
            response = dynamicCommand.command.response;

        response = this._replaceDynamicContent(response, context);

        respond(response);
    }

    _checkCommandSyntax(command) {
        return command.responses instanceof Array ||
            typeof command.responses === 'string' ||
            typeof command.response === 'string';
    }

    /**
     * Replace the user-specific content in the responses. This will execute a string replace on the response text to fill in any dynamic content within
     * @param {string} response
     * @param {UserContext} context
     * @return {String}
     * @protected
     */
    _replaceDynamicContent(response, context) {
        response = response.replaceAll('#username#', `@${context.username}`);

        return response;
    }
}

module.exports = DynamicCommands;