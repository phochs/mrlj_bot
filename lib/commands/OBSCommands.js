const MultiCommand = require('../MultiCommand');
const log4js = require("log4js");
const logger = log4js.getLogger();
const OBSClient = require('../OBSClient');
const random = require("random");
const async = require('async');

const OBS_ENABLED = process.env.OBS_ENABLED ? process.env.OBS_ENABLED.toLowerCase() === 'true' : false;

/**
 * Manages all dynamic commands which interact with an OBS client. The address and credentials are set in the environment
 * variables. The commands are read from the res/OBSCommands.json file. It will automatically update the command set
 * whenever the file is changed. Within each command there are 1 or more responses, which are executed in sequence.
 */
class OBSCommands extends MultiCommand {
    constructor(client) {
        super(client);

        this._useFile = true;
        this._commandsFile = 'res/OBSCommands.json';
        this._obs = null;
    }

    init() {
        this._obs = new OBSClient();

        this._obs.on('connected', () => {
            this._loadDynamicCommands().catch(e => {
                logger.error(e);
            });
        });

        if (OBS_ENABLED) {
            return this._obs.init()
                .then(() => {
                    return super.init();
                });

        } else {
            return new Promise((resolve) => {
                resolve();
            });
        }
    }

    /**
     * Execute a dynamic command
     * @param {boolean|{index: number, command: Object}} dynamicCommand
     * @param {String[]} args A list of all arguments passed by the user
     * @param {String} channel The channel where the message was sent
     * @param {UserContext} context The user context object
     * @param {function} respond
     * @protected
     */
    _executeDynamicCommand(dynamicCommand, args, channel, context, respond) {
        if (dynamicCommand.index !== -1) {
            this._commands[dynamicCommand.index]._commandTimeout.start();

            // Go through each response 1 by 1, and execute it
            async.eachSeries(dynamicCommand.command.responses, (response, callback) => {
                try {
                    // If the command has a list of replies, select 1 and send it to the chat
                    if (response.messages instanceof Array) {
                        let responseText = response.messages[random.int(0, response.messages.length - 1)];
                        respond(responseText);
                    } else if (typeof response.messages === 'string') {
                        respond(response.messages);
                    } else if (typeof response.message === 'string') {
                        respond(response.message);
                    }

                    // Execute the response based on its type
                    if (this._obs.connected) {
                        switch (response.type) {
                            case 'sourceCycle':
                                logger.debug('Executing sourceCycle response');
                                this._executeCycleSource(response, callback);
                                break;
                            case 'sourceToggle':
                                logger.debug('Executing sourceToggle response');
                                this._executeToggleSource(response, callback);
                                break;
                            case 'sourceEnable':
                                logger.debug('Executing sourceEnable response');
                                this._executeSetSourceStateCommand(true, response, callback);
                                break;
                            case 'sourceDisable':
                                logger.debug('Executing sourceDisable response');
                                this._executeSetSourceStateCommand(false, response, callback);
                                break;
                            case 'sceneSwitch':
                                logger.debug('Executing sceneSwitch response');
                                this._executeSwitchSceneCommand(response, callback);
                                break;
                            case 'pause':
                                logger.debug('Executing pause response');
                                this._executePause(response, callback);
                                break;
                            case 'null':
                                logger.debug('Executing null response');
                                callback();
                                break;
                            default:
                                logger.warn(`No valid response type found for ${response.type}`);
                                callback();
                                break;
                        }
                    } else {
                        logger.debug(`OBS not connected, skipping ${response.type}`);

                        callback();
                    }
                } catch (e) {
                    logger.error(e);

                    callback();
                }
            }, e => {
                if (e)
                    logger.error(e);

                this._commands[dynamicCommand.index]._commandTimeout.finish();
            });
        }
    }

    /**
     * Execute an OBS cycle source response
     * @param {{messages: String[], scene: String, source: String, type: String, duration: number, OBSSceneItemId: number}} response
     * @param {function} callback
     * @protected
     */
    _executeCycleSource(response, callback) {
        if (!response.OBSSceneItemId) {
            logger.warn('No source provided in response');

            callback();
            return;
        }
        if (!response.scene) {
            logger.warn('No scene provided in response');

            callback();
            return;
        }
        if (!response.duration) {
            logger.warn('No duration provided in response');

            callback();
            return;
        }

        try {
            // Enable the source
            this._obs.client.call('SetSceneItemEnabled', {
                sceneName: response.scene,
                sceneItemId: response.OBSSceneItemId,
                sceneItemEnabled: true,
            }).then(() => {
                // Disable the source after the set timeout
                setTimeout(() => {
                    this._obs.client.call('SetSceneItemEnabled', {
                        sceneName: response.scene,
                        sceneItemId: response.OBSSceneItemId,
                        sceneItemEnabled: false,
                    }).catch(e => {
                        logger.error(e);
                    });

                    callback();
                }, response.duration);
            }).catch(e => {
                logger.error(e);

                callback();
            });
        } catch (e) {
            logger.error(e);

            callback();
        }
    }

    /**
     * Execute an OBS toggle source response
     * @param {{messages: String[], scene: String, source: String, type: String, OBSSceneItemId: number}} response
     * @param {function} callback
     * @protected
     */
    _executeToggleSource(response, callback) {
        if (!response.scene) {
            logger.warn('No scene provided in response');

            callback();
            return;
        }
        if (!response.OBSSceneItemId) {
            logger.warn('No source provided in response');

            callback();
            return;
        }

        try {
            this._obs.client.call('GetSceneItemEnabled', {
                sceneName: response.scene,
                sceneItemId: response.OBSSceneItemId,
            }).then(data => {
                this._obs.client.call('SetSceneItemEnabled', {
                    sceneName: response.scene,
                    sceneItemId: response.OBSSceneItemId,
                    sceneItemEnabled: !data.sceneItemEnabled,
                }).then(() => {
                    callback();
                }).catch(e => {
                    logger.error(e);

                    callback();
                });
            }).catch(e => {
                logger.error(e);

                callback();
            });
        } catch (e) {
            logger.error(e);

            callback();
        }
    }

    /**
     * Execute an OBS set source state response, either enabling or disabling the source
     * @param {boolean} state
     * @param {{messages: String[], scene: String, source: String, type: String, OBSSceneItemId: number}} response
     * @param {function} callback
     * @protected
     */
    _executeSetSourceStateCommand(state, response, callback) {
        if (!response.scene) {
            logger.warn('No scene provided in response');

            callback();
            return;
        }
        if (!response.OBSSceneItemId) {
            logger.warn('No source provided in response');

            callback();
            return;
        }

        try {
            state = state === true;

            this._obs.client.call('SetSceneItemEnabled', {
                sceneName: response.scene,
                sceneItemId: response.OBSSceneItemId,
                sceneItemEnabled: state,
            }).then(() => {
                callback();
            }).catch(e => {
                logger.error(e);

                callback();
            });
        } catch (e) {
            logger.error(e);

            callback();
        }
    }

    /**
     * Execute an OBS switch scene response
     * @param {{messages: String[], scene: String, type: String}} response
     * @param {function} callback
     * @protected
     */
    _executeSwitchSceneCommand(response, callback) {
        if (!response.scene) {
            logger.warn('No scene name provided in response');

            callback();
            return;
        }

        try {
            this._obs.client.call('SetCurrentProgramScene', {
                sceneName: response.scene,
            }).then(() => {
                callback();
            }).catch(e => {
                logger.error(e);

                callback();
            });
        } catch (e) {
            logger.error(e);

            callback();
        }
    }

    /**
     * Execute pause response
     * @param {{messages: String[], type: String, duration: number}} response
     * @param {function} callback
     * @protected
     */
    _executePause(response, callback) {
        if (!response.duration) {
            logger.warn('No duration provided in response');

            callback();
            return;
        }

        if (typeof response.duration !== 'number')
            response.duration = parseInt(response.duration);

        setTimeout(() => {
            callback();
        }, response.duration);
    }

    /**
     * Checks if the command contains a valid response
     * @param {Object} command the parsed JSON for a single command
     * @return {boolean} True if the command is correct syntax
     * @protected
     */
    _checkCommandSyntax(command) {
        return command.responses instanceof Array;
    }

    /**
     * Look up the source ID for every response that has a source attribute provided
     * @param {Object} command
     * @param {function} callback
     * @protected
     */
    _fulfillCommand(command, callback) {
        let responses = [];

        async.eachSeries(command.responses, (response, callback) => {
            if (response.source) {
                this._obs.client.call('GetSceneItemId', {
                    sceneName: response.scene,
                    sourceName: response.source,
                }).then(data => {
                    response.OBSSceneItemId = data.sceneItemId;
                    responses.push(response);

                    callback();
                }).catch(e => {
                    logger.error(e);

                    callback();
                });
            } else {
                responses.push(response);
                callback();
            }
        }, e => {
            if (e)
                logger.error(e);

            command.responses = responses;

            callback(command);
        });
    }
}

module.exports = OBSCommands;