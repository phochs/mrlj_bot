const Command = require('./Command');
const CommandTimeout = require('./CommandTimeout');
const UserContext = require('./UserContext');
const fs = require("fs");
const chokidar = require('chokidar');
const async = require('async');
const log4js = require("log4js");
const logger = log4js.getLogger();

/**
 * Extends the Command class to allow for a dynamic list of commands. Usually used by reading a JSON file containing
 * the commands
 * @abstract
 */
class MultiCommand extends Command {
    constructor(client) {
        super(client);

        this._useFile = false;
        this._commandsFile = '';
        this._commands = [];
        this._fileWatcher = null;
        this._messageCount = 0;
    }

    init() {
        this._client.on('message', () => {
            this._messageCount++;
        });

        if (this._useFile) {
            return this._loadDynamicCommands().then(() => {
                return new Promise((resolve) => {
                    this._dynamicCommandListener();

                    resolve();
                });
            });
        } else {
            return null;
        }
    }

    /**
     * Checks if a valid command exists in the commands list
     * @param {String} command The command the user entered, including any prefixes like an exclamation mark
     * @param {String[]} args A list of all arguments passed by the user
     * @param {UserContext} context The user context object
     * @return {boolean} True if the command is valid for this class, false if not
     * @protected
     */
    _commandTest(command, args, context) {
        return this._findCommand(command, args, context) !== false;
    }

    /**
     * Execute the dynamic command
     * @param {String} command The command the user entered, including any prefixes like an exclamation mark
     * @param {String[]} args A list of all arguments passed by the user
     * @param {String} channel The channel where the message was sent
     * @param {UserContext} context The user context object
     * @param {function} respond
     */
    execute(command, args, channel, context, respond) {
        let dynamicCommand = this._findCommand(command, args, context);

        if (dynamicCommand !== false) {
            // Reset message count for command to current message
            this._commands[dynamicCommand.index]._messageCount = this._messageCount;

            this._executeDynamicCommand(dynamicCommand, args, channel, context, respond);
        }
    }

    /**
     * Checks if a single command JSON object is valid for this class
     * @param {Object} command the parsed JSON for a single command
     * @return {boolean} True if the command is correct syntax
     * @protected
     */
    _checkCommandSyntax(command) {
        return true;
    }

    /**
     * Any child classes can use this method to add information to a command, before it gets stored in the this._commands
     * array. when done, it calls callback(command)
     * @param {Object} command
     * @param {function} callback
     * @protected
     */
    _fulfillCommand(command, callback) {
        callback(command);
    }

    /**
     * Execute a dynamic command
     * @param {boolean|{index: number, command: Object}} dynamicCommand
     * @param {String[]} args A list of all arguments passed by the user
     * @param {String} channel The channel where the message was sent
     * @param {UserContext} context The user context object
     * @param {function} respond
     * @protected
     * @abstract
     */
    _executeDynamicCommand(dynamicCommand, args, channel, context, respond) {

    }

    /**
     * (Re)load the list of dynamic commands from the JSON file.
     * @return {Promise<unknown>}
     * @protected
     */
    _loadDynamicCommands() {
        return new Promise((resolve, reject) => {
            // Read the JSON file as text
            fs.readFile(this._commandsFile, 'utf8', (e, data) => {
                try {
                    if (e) {
                        reject(e);
                    } else {
                        const commands = JSON.parse(data);
                        if (commands.commands) {
                            let newCommandList = [];

                            async.eachSeries(commands.commands, (JSONCommand, callback) => {
                                try {
                                    if (!JSONCommand.command instanceof String) // Ignore commands whose name isn't a string
                                        callback();
                                    else if (!this._checkCommandSyntax(JSONCommand)) {
                                        callback();
                                    } else {
                                        JSONCommand._commandTimeout = new CommandTimeout();
                                        JSONCommand._messageCount = this._messageCount;

                                        if (JSONCommand.timeout !== undefined) {
                                            JSONCommand._commandTimeout.active = true;
                                            JSONCommand._commandTimeout.timeout = parseInt(JSONCommand.timeout);

                                            if (JSONCommand.assumeFailed !== undefined) {
                                                JSONCommand._commandTimeout.assumeFailed = JSONCommand.assumeFailed;
                                            }
                                        }

                                        this._fulfillCommand(JSONCommand, command => {
                                            if (command)
                                                newCommandList.push(command);

                                            callback();
                                        });
                                    }
                                } catch (e) {
                                    logger.error(e);
                                }
                            }, e => {
                                if (e)
                                    logger.error(e);

                                try {
                                    // If the new list of commands is empty, we won't override the existing set
                                    if (newCommandList.length > 0) {

                                        // Clear intervals of the current command set
                                        for (let i = 0; i < this._commands.length; i++) {
                                            if (this._commands[i].timer) {
                                                if (this._commands[i].timer._interval) {
                                                    clearInterval(this._commands[i].timer._interval);
                                                }
                                            }
                                        }

                                        this._commands = newCommandList;

                                        // Set intervals for the new command set
                                        for (let i = 0; i < this._commands.length; i++) {
                                            if (this._commands[i].timer) {
                                                if (this._commands[i].timer.interval > 0) {
                                                    let interval = this._commands[i].timer.interval;

                                                    this._commands[i].timer._interval = setInterval(() => {
                                                        this._executeTimedCommand(i);
                                                    }, interval);

                                                    if (this._commands[i].timer.messages === undefined)
                                                        this._commands[i].timer.messages = 0;
                                                }
                                            }
                                        }

                                        logger.info('Published new command set');
                                    } else {
                                        logger.warn('No valid commands found, keeping current command list');
                                    }

                                    resolve();
                                } catch (e) {
                                    logger.error(e);
                                }
                            });
                        } else {
                            reject();
                        }
                    }
                } catch (e) {
                    reject(e);
                }
            });
        });
    }

    /**
     * Set up a listener for changes to the dynamicCommands.json file.
     * @protected
     */
    _dynamicCommandListener() {
        try {
            this._fileWatcher = chokidar.watch(this._commandsFile, {persistent: true});

            this._fileWatcher
                .on('add', () => {
                    logger.debug('File was added. Reloading...');
                    this._loadDynamicCommands().catch((e) => {
                        logger.error(e);
                    });
                })
                .on('change', () => {
                    logger.debug('File was changed. Reloading...');
                    this._loadDynamicCommands().catch((e) => {
                        logger.error(e);
                    });
                });
        } catch (e) {
            logger.error(e);
        }
    }

    /**
     * Tries to find a compatible command based on the command the user entered, and it's parameters
     * @param {String} command The command the user entered, including any prefixes like an exclamation mark
     * @param {Array} args A list of all arguments passed by the user
     * @param {Object} context The user context object
     * @return {boolean|{index: number, command: Object}}
     * @protected
     */
    _findCommand(command, args, context) {
        for (let i = 0; i < this._commands.length; i++) {
            if (command === this._commands[i].command) {
                if (this._commands[i].strict === true && args.length > 0)
                    continue;

                if (!this._commands[i]._commandTimeout.test(context))
                    continue;

                return {index: i, command: this._commands[i]};
            }
        }

        return false;
    }

    _executeTimedCommand(index) {
        let command = {
            index: index,
            command: this._commands[index],
        };

        if (this._messageCount >= command.command._messageCount + command.command.timer.messages) {
            logger.debug(`Executing timed command ${command.command.command}`);

            this._commands[index]._messageCount = this._messageCount;

            this._executeDynamicCommand(command, [], 'timer', new UserContext(), text => {
                this._client.say(process.env.CLIENT_CHANNEL, text);
            });
        }
    }
}

module.exports = MultiCommand;