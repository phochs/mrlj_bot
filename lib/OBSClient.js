const EventEmitter = require('events');
const OBSWebSocket = require('obs-websocket-js').default;
const log4js = require("log4js");
const logger = log4js.getLogger();

/**
 * The maximum number of attempts the websocket client will perform before quitting. Set to -1 for infinite attempts
 * @type {number}
 */
const MAX_ATTEMPTS = parseInt(process.env.OBS_MAX_ATTEMPTS) || 10;
/**
 * The amount of time the client will wait before reconnecting
 * @type {number}
 */
const RECONNECT_TIMEOUT = parseInt(process.env.OBS_RECONNECT_TIMEOUT) || 1000;

/**
 * OBSClient creates a connection with an OBS instance to exchange commands over websockets.
 */
class OBSClient extends EventEmitter {
    constructor() {
        super();

        /**
         * If the client is connected to an OBS instance
         * @type {boolean}
         * @readonly
         */
        this.connected = false;
        /**
         * The OBS websocket client
         * @type {?OBSWebSocket}
         */
        this.client = null;
        /**
         * Internal count for the amount of failed connections. Gets reset to 0 when a connection is successful
         * @type {number}
         * @protected
         */
        this._connectionFailCount = 0;
    }

    /**
     * Initializes the OBS client, by registering the event handlers and connecting to OBS
     * @return {Promise}
     */
    init() {
        this.client = new OBSWebSocket();

        this._setListeners();

        return this._connectOBS();
    }

    /**
     * Sets up all required listeners for the OBS client. These make sure the this.connected value is set correctly, and
     * make the client automatically reconnect if it loses connection.
     * @protected
     */
    _setListeners() {
        this.client.on('ConnectionOpened', () => {
            logger.debug('OBS connection opened');
            this.connected = true;
            this._connectionFailCount = 0;

            // Give the websocket some (currently hardcoded) time to finalize the connection. It's not instantly ready when ConnectionOpened is fired
            setTimeout(() => {
                this.emit('connected');
            }, 500);
        });
        this.client.on('ConnectionClosed', (webSocketError) => {
            logger.error('OBS connection closed');
            logger.error(webSocketError);

            this.connected = false;
            this._connectionFailCount = this._connectionFailCount + 1;

            // Automatically reconnect
            setTimeout(() => {
                this._connectOBS();
            }, RECONNECT_TIMEOUT);

            this.emit('disconnected');
        });
    }

    /**
     * Make a connection to OBS. This will set up the websocket connection, except if there have been too many
     * connection failures.
     * @return {Promise<any>|boolean}
     * @protected
     */
    _connectOBS() {
        if (MAX_ATTEMPTS < 0 || this._connectionFailCount < MAX_ATTEMPTS) {
            let password = undefined;

            if (process.env.OBS_PASSWORD)
                password = process.env.OBS_PASSWORD;

            return this.client.connect(process.env.OBS_ADDRESS, password).catch(e => {
                logger.error(e);
            });
        } else {
            logger.error('Too many OBS connection failures, stopping reconnect');

            return false;
        }
    }
}

module.exports = OBSClient;