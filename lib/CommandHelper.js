class CommandHelper {
    static parseCommandString(commandString) {
        commandString = commandString.trim();

        let commandList = commandString.split(' '); // All the command arguments (separated by a space)
        let command = commandList.shift().toLowerCase(); // The command (including any prefix like ! )

        return {
            command: command,
            args: commandList,
        };
    }
}

module.exports = CommandHelper;