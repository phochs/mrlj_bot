const express = require('express');
const log4js = require("log4js");
const logger = log4js.getLogger();
const client = require('prom-client');
const collectDefaultMetrics = client.collectDefaultMetrics;
const register = client.register;

/**
 * Creates a simple webserver to provide Prometheus metrics of the bot.
 */
class Webserver {
    constructor() {
        this.app = null;
    }

    init() {
        try {
            collectDefaultMetrics();
        } catch (e) {
            logger.warn(e);
        }

        return new Promise((resolve, reject) => {
            this.app = express();

            this.app.get('/', (req, res) => {
                try {
                    res.send('Awkward wave');
                } catch (e) {
                    logger.error(e);
                }
            });

            this.app.get('/metrics', async (req, res) => {
                try {
                    res.set('Content-Type', register.contentType);
                    res.end(await register.metrics());
                } catch (e) {
                    logger.error(e);
                    res.status(500).end(e);
                }
            });

            this.app.listen(8080, () => {
                resolve();
                logger.debug(`Web service is running on port 8080!`);
            });
        });
    }
}

module.exports = Webserver;