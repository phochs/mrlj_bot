/**
 * The LJ bot is a custom-made bot for the official_mrlj Twitch channel. It mainly supports executing commands, but it
 * can do that in a variety of ways.
 * In principle, all commands are defined as classes inside the commands/ folder. There are a few special classes in
 * there for dynamic commands (they are defined in res/dynamicCommands.json), commands that trigger OBS sources (those
 * are defined on res/OBSCommands.json). Some other classes interact with external APIs and such.
 *
 * The script scans every incoming chat message, regardless of content. Because some commands don't have a prefix like
 * an exclamation mark, every chat message has to be analyzed.
 *
 * In this file, the Twitch client, web server, and command handler are initialized. They handle all functions of the bot.
 *
 * @author phochs <phochs@runbox.com>
 * @license MIT
 * @version 0.2.3
 */
process.env.APP_VERSION = '0.2.3';

const TwitchClient = require('./lib/TwitchClient');
const CommandHandler = require('./lib/CommandHandler');
const Webserver = require('./lib/Webserver');
const log4js = require("log4js");
const logger = log4js.getLogger();

/**
 * This reads the environment variables from the .env file, if present. If not, these variables should be passed via
 * other means, like the ENV values in DOcker.
 */
require('dotenv').config();

/**
 * This section configures the Log4js library. All other classes and libraries that get called directly or indirectly
 * from here can import log4js, and they will automatically get the correct configuration.
 */
log4js.configure({
    appenders: {
        out: {type: "stdout"},
        file: {type: "file", filename: "logs/main.log"},
    },
    categories: {
        default: {appenders: ["out", "file"], level: "debug"},
    },
});

/**
 * We need to start the Twitch client, the web server, and the command handler.
 */
let twitchClient = new TwitchClient();
let webserver = new Webserver();
let commandHandler = new CommandHandler(twitchClient);
twitchClient.init().then(() => {
    return webserver.init();
}).then(() => {
    return commandHandler.init();
}).catch(e => {
    logger.error(e);
    throw e;
});
